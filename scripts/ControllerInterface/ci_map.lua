local ui = require("openmw.ui")
local I = require("openmw.interfaces")
local v2 = require("openmw.util").vector2
local util = require("openmw.util")
local input = require("openmw.input")
local self = require("openmw.self")
local mapSize = 4096
local mult = mapSize / 2048
local drawScale = 1
local drawSize = 512
local screenHeight = ui.layers[1].size.y
local subSectionSize = { width = screenHeight * drawScale, height = screenHeight * drawScale }
local startX, startY = 0, 0
local scale = 0.003 * mult
local mapPath = "textures/CI/AnnotatedOutlanderMap.dds"--dr_map_morrowind_01.dds"
local zeroPosition = v2(mapSize / 2 - (160 * mult), (mapSize / 2) - (80 * mult))
local mapElement
local function getMarkerPositionInSubsection(markerX, markerY, startX, startY)
    -- Check if the marker is within the visible subsection
    if markerX >= startX and markerX < startX + subSectionSize.width and
        markerY >= startY and markerY < startY + subSectionSize.height then
        -- Calculate the position relative to the top-left corner of the subsection
        local relativeX = markerX - startX
        local relativeY = markerY - startY
        return relativeX, relativeY
    else
        -- The marker is outside the visible subsection
        return nil
    end
end
local function getMapPosFromRealPos(x, y)
    local mapX = zeroPosition.x + (x * scale)
    local mapY = zeroPosition.y - (y * scale)

    return util.vector2(mapX, mapY)
end
local function imageContent(resource, size)
    return {
        type = ui.TYPE.Image,
        props = {
            resource = resource,
            size = util.vector2(size, size),
            position = util.vector2(0, 0),
            anchor = util.vector2(0, 0),
        }
    }
end
local function textContent(text, position)
    return {
        type = ui.TYPE.Text,
        template = I.MWUI.templates.textNormal,
        props = {
            text = text,
            textSize = 10,
            position = position,
            textColor = util.color.rgba(0, 0, 0, 1)
        }
    }
end
local mapMarkers = {
    worldOrigin = { x = 0, y = 0 },
    Vos = { x = 100770.71875, y = 114190 },
    Firewatch = { x = 141798.890625, y = 125519.2734375 },
    Vivec = { x = 32611.587890625, y = -72525.3125 },
    ["Ebonheart"] = { x = 21221.919921875, y = -103324.8046875 },
    ["Old Ebonheart"] = { x = 63880.11328125, y = -150133.1875 },
    Balmora = { x = -21226.6484375, y = -18292.150390625 }
}
local function renderMap(item, bold, icon)
    local resource = ui.texture {
        path = mapPath,
        offset = v2(startX, startY),
        size = v2(screenHeight, screenHeight)
    }

    local content = {}
    local mapMarkerCopy = {}
    for index, value in pairs(mapMarkers) do
       mapMarkerCopy[index] = value
    end
    mapMarkerCopy["Player"] = {x  = self.position.x, y = self.position.y}
    for key, value in pairs(mapMarkerCopy) do
        local Position = util.vector2(value.x, value.y)
        local correctedPosition = getMapPosFromRealPos(Position.x, Position.y)
        local relativeX, relativeY = getMarkerPositionInSubsection(correctedPosition.x, correctedPosition.y,
            startX, startY)
        if relativeX ~= nil then
            table.insert(content, textContent(key, v2(relativeX, relativeY)))--map marker
        end
    end

    table.insert(content, imageContent(resource, screenHeight ))--actual map
    return {
        layer = 'HUD',
        type = ui.TYPE.Container,
        props = {
            relativePosition = v2(0.5, 0.5),
            anchor = v2(0.5, 0.5),
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                content = ui.content(content)
            }
        }
    }
end
local function drawMap()
    local icontent = renderMap()
    if mapElement then
        mapElement:destroy()
    end
    mapElement = ui.create(icontent)
end
local modifier = 100
local function onKeyPress(key)
    if not mapElement then
        return
    end
    local modifierToUse = modifier
    if input.isShiftPressed() then
        modifierToUse = modifierToUse / 10
    end
    if key.code == input.KEY.LeftArrow then
        startX = startX - modifierToUse
    elseif key.code == input.KEY.RightArrow then
        startX = startX + modifierToUse
    elseif key.code == input.KEY.UpArrow then
        startY = startY - modifierToUse
    elseif key.code == input.KEY.DownArrow then
        startY = startY + modifierToUse
    end
    drawMap()
end
local function UiModeChanged(data)
    if not data.newMode then
        if mapElement then
            mapElement:destroy()
            mapElement = nil
        end
    end
end
local lastCell
local function onUpdate(dt)
    if self.cell ~= lastCell and self.cell.name ~= "" and self.cell.name ~= nil then
        lastCell = self.cell
        if not mapMarkers[self.cell.name] and self.cell.isExterior then
            mapMarkers[self.cell.name] = { x = self.position.x, y = self.position.y }
        end

    end
end

local function onSave()
    return {
        mapMarkers = mapMarkers,
        startX = startX,
        startY = startY
    }
end

local function onLoad(data)
    if not data then
        return
    end
    mapMarkers = data.mapMarkers
    startX = data.startX or 0
    startY = data.startY or 0

end
local scrollModifier = 10
local function onMouseWheel(vertical, horizontal)
    if mapElement == nil then
        return
    end
    startX = startX + horizontal * scrollModifier
    startY = startY + -vertical * scrollModifier
    drawMap()
end
local function onFrame(dt)
    if not mapElement then
        return
    end
    local axisX = input.getAxisValue(input.CONTROLLER_AXIS.LeftX)
    local axisY = input.getAxisValue(input.CONTROLLER_AXIS.LeftY)
    if axisX ~= 0 or axisY ~= 0 then
        -- Apply deadzone
        local deadzone = 0.1
        if math.abs(axisX) < deadzone then
            axisX = 0
        end
        if math.abs(axisY) < deadzone then
            axisY = 0
        end
        
        startX = startX + axisX * scrollModifier
        startY = startY + axisY * scrollModifier
        drawMap()
    end
end
return {
    interfaceName = "Map",
    interface = {
        drawMap = drawMap
    },
    engineHandlers = {
        onKeyPress = onKeyPress,
        onSave = onSave,
        onFrame = onFrame,
        onLoad = onLoad,
        onUpdate = onUpdate,
        onMouseWheel = onMouseWheel,
    },
    eventHandlers = {
        UiModeChanged = UiModeChanged
    }
}
