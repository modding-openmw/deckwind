local I = require("openmw.interfaces")
local storage = require("openmw.storage")

local key = "SettingsControllerInterface"
local globalSettings = storage.globalSection(key)
I.Settings.registerGroup {
    key = key,
    page = key,
    l10n = "ControllerInterface",
    name = "Deckwind",
    description = "",
    permanentStorage = true,
    settings = {
        {
            key = "enableMod",
            renderer = "checkbox",
            name = "Enable Deckwind",
            description = "When changing this option, you will need to restart the game for it to take effect.",
            default = true
        },
        {
            key = "defaultDialogCursor",
            renderer = "checkbox",
            name = "Default Dialog Window to Cursor mode",
            default = true
        },
        {
            key = "useNewStatsWindow",
            renderer = "checkbox",
            name = "Use New Stats Window",
            default = false
        },
        {
            key = "useNewMapWindow",
            renderer = "checkbox",
            name = "Use New Map Window",
            default = false
        },
        {
            key = "useInventoryinListMode",
            renderer = "checkbox",
            name = "Use List Mode for inventory items",
            description = "Switches to list mode for inv items, instead of a grid. Experimental.",
            default = false
        },
        {
            key = "useSuppliedIcons",
            renderer = "checkbox",
            name = "Use Provided Magic Icons",
            description = "Deckwind comes with a set of larger icons for magic effects. If you prefer to use the default icon paths, disable this option. You may want to do this if you have your own icon replacer.",
            default = true
        },
        {
            key = "iconSize",
            renderer = "number",
            name = "Icon Size",
            default = 40
        },
        {
            key = "gridSizeContainer",
            renderer = "number",
            name = "Container Grid Size",
            default = 10
        },
        {
            key = "gridSizeInventory",
            renderer = "number",
            name = "Inventory Grid Size",
            default = 10
        },
        {
            key = "magicListLength",
            renderer = "number",
            name = "Magic Item List Length",
            default = 10
        },
    }
}